# Link Shortener Application



### Functionalities
- secured google auth, user can log in using their google account (authorization code flow)
- user can create a shortened link
- user can disable a link
- shows total clicks for each shortened link generated

### Services
| Service | Function | stack | 
| - | - | - |
| client | serves spa client | react/svelte | 
| api | rest api, managed links | c# .net 5 |
| redirect | exposed to public, redirects client to the link mapped to the shortened link | golang, groupcache |
