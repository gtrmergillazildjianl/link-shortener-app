
using Microsoft.Extensions.DependencyInjection;


namespace LinkShortenerLib.Services {

    
    // LinkShortener Service
    // @todo implement service
    public static class LinkShortenerServiceExtensions {
        
        public static IServiceCollection AddLinkShortenerService(this IServiceCollection serviceCollection) {
            serviceCollection.AddSingleton<LinkShortenerService>();
            return serviceCollection;
        }
    }
}